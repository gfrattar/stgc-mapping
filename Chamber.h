class Chamber {

  public:
    
    Chamber(string _chamber_type){
      chamber_type = _chamber_type;
      InitializeMap();
      Produce_IDVMM_map();
      Produce_Updated_Pad_Mapping();
    }


    string chamber_type;

    map<TString,int> startID;
    map<TString,int> VMM_chan_min;
    map<TString,int> VMM_chan_max;

    // Store for each electronic channel the ID of the pad
    // in the _excel file_ scheme (old scheme)
    // which need to be converted via Get Updated Pad Mapping
    map< int, map<int,int> > padID;

    // This map stores the conversion to
    // updated (R,phi) numbering
    map<int, int> updated_padID;

    // Store for each padID the radius and phi
    map< int, int> pad_radius;
    map< int, int> pad_phi;

    //Methods
    void InitializeMap(void);
    bool IsPosIDs(void);
    void Produce_IDVMM_map(void);
    void Produce_Updated_Pad_Mapping(void);
    void Print_IDVMM_map(void);
    
    //Debug function
    void Check_VMM_to_updated_mapping(void);

    /*
    // map between electronics channel and old sectors
    map< int, map<int,int> > Get_IDVMM_map(void);

    // map between old sectors and new sectors
    map<int, int> Get_Updated_Pad_Mapping(void);
    */

};
