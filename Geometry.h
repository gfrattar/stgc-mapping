class Geometry{
  
  public:

    static Geometry& get_istance(void){
      static Geometry istance;
      return istance;
    }

    int get_radius_divisions(TString chamber){
      if(chamber.Contains("QS1")) return 17;
      else return 0;
    } 

    int get_phi_divisions(TString chamber){
      if(chamber.Contains("P1") || chamber.Contains("P2")) return 4;
      else if(chamber.Contains("P3") || chamber.Contains("P4")) return 3;
      else return 0;
    } 

    int get_height_central_pads(TString chamber){
      if(chamber.Contains("QS1")) return 13;
      else return 0;
    }

    int get_height_first_pad(TString chamber){
      if(chamber.Contains("P1") || chamber.Contains("P2")) return 11;  
      else if(chamber.Contains("P3") || chamber.Contains("P4")) return 5;
      else return 0;
    }

    int get_height_last_pad(TString chamber){
      if(chamber.Contains("P1") || chamber.Contains("P2")) return 5;  
      else if(chamber.Contains("P3") || chamber.Contains("P4")) return 11;
      else return 0;
    }

    int get_height(TString chamber){
      if(chamber.Contains("QS")) return 211;
      else return 0;
    }

    int get_high_base(TString chamber){
      if(chamber.Contains("QS")) return 108;
      else return 0;
    }

    int get_low_base(TString chamber){
      if(chamber.Contains("QS")) return 48;
      else return 0;
    }

    int get_N_pads(TString chamber){
      if(chamber == "QS1P1" || chamber == "QS1P2") return 68;
      else if(chamber == "QS1P3" || chamber == "QS1P4") return 51;
      else return 0;
    }

    void build_master_points(TString chamber){

      int t_low_base = chamber.Contains("QS") ? 48 : 0;
      int t_high_base = chamber.Contains("QS") ? 108 : 0;
      int t_height = chamber.Contains("QS") ? 211 : 0;

      this->master_11_x = this->x_offset + this->x_offset_low;
      this->master_11_y = this->y_offset;

      this->master_21_x = this->master_11_x + t_low_base;
      this->master_21_y = this->master_11_y;

      this->master_12_x = 0 + this->x_offset;
      this->master_12_y = t_height + this->y_offset;

      this->master_22_x = 0 + this->x_offset + t_high_base;
      this->master_22_y = this->master_12_y;

    }

    float GetX_givenRPhi(TString chamber, int leftORright, int phi, float R);


    int x_offset     =  10;
    int x_offset_low =  30;
    int y_offset     =  20;

    float master_11_x;
    float master_11_y;

    float master_21_x;
    float master_21_y;

    float master_12_x;
    float master_12_y;

    float master_22_x;
    float master_22_y;

  private:
    Geometry(){};

};
