class Pad {

  public:
  
    Pad(string _chamber_type, int _ID, int _R, int _phi){
      chamber_type = _chamber_type;
      ID = _ID;
      R = _R;
      phi = _phi;
      Assign_Y();
      Assign_X();
      Correct_Y();
      xcoordinates = {x1,x2,x4,x3};
      ycoordinates = {y1,y1,y2,y2};
    }

    double x1;
    double x2;
    double x3;
    double x4;
    double y1;
    double y2;

    vector<Double_t> xcoordinates;
    vector<Double_t> ycoordinates;

    int R;
    int phi;
    int ID;

    float rate;

    string chamber_type;

    //map<string, map<unsigned int, unsigned int > > M;
    //padID[vmmid][vmm_channel] = ID;

    // Methods
    void Assign_X(void);
    void Assign_Y(void);
    float CalculateXmin(float yeval);
    float CalculateXmax(float yeval);
    float CalculateX(float px1, float px2, float py1, float py2, float yeval, int which);
    void Correct_Y(void);

};
