void MakePlot(vector<Pad*> the_pads, TString chamber_name){

  //These basically determine the dimension of the canvas and can be tuned to include additional chambers
  int xbins = Geometry::get_istance().x_offset * 2 + 108;
  int ybins = Geometry::get_istance().y_offset * 2 + 211;

  TH2Poly *layer = new TH2Poly();
  layer->SetLineColor(kBlack);
  for(int i = 0; i < the_pads.size(); i++){
    layer->AddBin(4, &(the_pads.at(i)->xcoordinates[0]), &(the_pads.at(i)->ycoordinates[0]));
    layer->SetBinContent(i+1,i+1);
  }

  TCanvas *canvas = new TCanvas("c","",600,600);
  canvas->SetLeftMargin(0.15);
  canvas->SetTicks(1,1);

  TH2F* empty = new TH2F("empty",";x / 6 [mm];y / 6 [mm]", xbins, 0, xbins, ybins, 0, ybins);
  empty->Draw();

  layer->Draw("colzL same");


  TString eospath = "/eos/home-g/gfrattar/www/NSW/";

  canvas->SaveAs(eospath+"/"+chamber_name+".pdf");
  canvas->SaveAs(eospath+"/"+chamber_name+".png");

}
